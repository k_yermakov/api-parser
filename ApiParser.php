<?php

/**
 * Created by PhpStorm.
 * User: ermakov
 * Date: 18.04.17
 * Time: 13:09
 */

include "Logger.php";

class ApiParser
{
    /**
     * @var Connection to DB
     */
    private $dbConn;

    /**
     * @var array Access Denied
     */
    public $blacklistProxies = [];

    /**
     * @var Не реагирующие прокси
     */
    public $unresponsiveProxies;

    /**
     * @var Максимальное количество попыток подключения к прокси
     */
    private $maxConnectionAttempts;

    /**
     * @var Максимальное время ожидания от API сервера
     */
    private $connectionTimeout;

    /**
     * @var Максимальное время подключения к прокси
     */
    private $timeout;

    /**
     * @var array Массив текущих url для обработки
     */
    private $urls;

    /**
     * @var Выборка прокси (из переменной $proxies), которые будут использоваться для мультизапросов
     *      5 прокси = к одновременных запросов
     */
    private $proxySelection;

    private $proxies = [];

    private $proxiesOriginal = [];

    private $currentProxyIds = [];

    private $currentProxyId = null;

    private $connectionAttempts = 0;

    public function __construct(
        $maxConnectionAttempts = 5,
        $connectionTimeout = 5,
        $timeout = 3,
        $maxProxySelection = 5,
        $proxies
    ) {
        $this->maxConnectionAttempts = $maxConnectionAttempts;
        $this->connectionTimeout = $connectionTimeout;
        $this->timeout = $timeout;
        $this->maxProxySelection = $maxProxySelection;
        $this->proxies = $this->proxiesOriginal = $proxies;
    }

    public function setUrls($urls)
    {
        $this->urls = $urls;
    }

    public function getMultipleResponses(array $urls)
    {
        echo count($this->proxies) . "\n";

        $this->setUrls($urls);

        // build the requests
        $ch = [];
        $responses = [];
        $randProxies = $this->getProxyIds(count($urls));
        $mh = curl_multi_init();
        for ($i = 0; $i < count($urls); $i++) {
            $ch[$i] = $this->curlSetOptions($urls[$i], $this->proxies[$randProxies[$i]], $this->connectionTimeout, $this->timeout);

            curl_multi_add_handle($mh, $ch[$i]);
        }

        // execute the requests simultaneously
        $running = 0;
        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);

        for ($i = 0; $i < count($urls); $i++) {
            // $results contains this keyword's tweets as an associative array
            $content = curl_multi_getcontent($ch[$i]);

            $this->resetConnectionAttempts();

            Logger::log('log.txt',
                "URL: " . $urls[$i] . "\n");
            Logger::log('log.txt',
                "Всего живых прокси getMultipleResponses: " . count($this->proxies) . "\n");
            Logger::log('log.txt',
                "Всего должно быть прокси: " . count($urls) . "\n");
            if ($content === false || $content == "") {
                /*Logger::log('log.txt',
                    "Error: " . curl_error($ch[$i]) . " proxy: " . $this->proxies[$randProxies[$i]] . " url: " . $urls[$i] . "\n");*/
                $responses[] = $this->getResponse($urls[$i], $randProxies[$i]);
            } elseif (json_decode($content) == false) {
                unset($this->proxies[$randProxies[$i]]);
                $responses[] = $this->getResponse($urls[$i]);
            } else {
                /*Logger::log('log.txt',
                    "Ответ получен. proxy: " . $this->proxies[$randProxies[$i]] . "\n");*/
                Logger::log('log.txt', "Ответ получен. \n");
                $responses[] = $content;
            }
        }

        Logger::log('log.txt',
            "Последний обработанный кадастровый номер: " . $urls[count($urls) - 1] . "\n");
        /*Logger::log('log.txt', "Ответ:" . implode("|",$responses ) .  "\n");*/
        return $responses;
    }

    public function getResponse($url, $proxyId = null)
    {
        if (!isset($proxyId)) {
            $proxyId = $this->getProxyId();
        }
        $proxy = $this->proxies[$proxyId];

        $ch = $this->curlSetOptions($url, $proxy, $this->connectionTimeout, $this->timeout);

        $content = curl_exec($ch);

        $this->connectionAttempts++;

        Logger::log('log.txt',
            "Всего живых прокси getResponse: " . count($this->proxies) . "\n");
        
        //Если количество прокси меньше количества url, то перезагрузим прокси
        if (count($this->proxies) < count($this->urls)) {
            throw new Exception('Проксей меньше, чем url');
        }

        if ($content === false) {
            curl_close($ch);
            if ($this->connectionAttempts >= $this->maxConnectionAttempts) {
                $this->resetConnectionAttempts();
                //return "{}";
                /*Logger::log('log.txt',
                    "Удаляем прокси в getResponse content === false: " . $this->proxies[$proxyId] . "\n");*/
                unset($this->proxies[$proxyId]);
                /* }*/
                return $this->getResponse($url);
            } else {
                return $this->getResponse($url, $proxyId);
            }
        } elseif (json_decode($content) == false) {
            //$this->blacklistProxies[] = $this->proxies[$proxyId];

            if ($this->connectionAttempts >= $this->maxConnectionAttempts) {
                $this->resetConnectionAttempts();
                return "{}";
            } else {
                unset($this->proxies[$proxyId]);
                return $this->getResponse($url);
            }
        } elseif ($content == "") {
            curl_close($ch);
            unset($this->proxies[$proxyId]);
            return $this->getResponse($url);
        } else {
            Logger::log('log.txt', "Ответ получен. \n");
            curl_close($ch);
            return $content;
        }
    }

    public function curlSetOptions($url, $proxy, $connectionTimeout, $timeout)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connectionTimeout);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

        return $ch;
    }

    private function getProxyIds($cnt)
    {
        if ($cnt > count($this->proxies)) {
            die($cnt . "Количество обрабатывающихся одновременно запросов не должно превышать количество допустимых прокси!");
        }
        $this->currentProxyIds = array_rand($this->proxies, $cnt);
        return $this->currentProxyIds;
    }

    private function getProxyId()
    {
        $this->currentProxyId = array_rand($this->proxies);
        return $this->currentProxyId;
    }

    private function resetConnectionAttempts()
    {
        $this->connectionAttempts = 0;
    }

    public function getActiveProxiesCnt()
    {
        return count($this->proxies);
    }

    public function setProxies($proxies)
    {
        $this->proxies = $this->proxiesOriginal = $proxies;
    }

    public function removeBadProxies()
    {
        $this->proxies = array_diff($this->proxies, $this->blacklistProxies);
    }
}