
<?php

/**
 * Created by PhpStorm.
 * User: ermakov
 * Date: 18.04.17
 * Time: 13:12
 */
class Cadastre
{
    /**
     * @var Номера кадастровых кварталов
     */
    private $cadastralQuarters;

    public function __construct($cadastralQuarters)
    {
        $this->cadastralQuarters = $cadastralQuarters;
    }

    public function getCadastralQuarters()
    {
        return $this->cadastralQuarters;
    }
}