<?php

/**
 * Created by PhpStorm.
 * User: ermakov
 * Date: 18.04.17
 * Time: 13:22
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
class Logger
{
    const logPath = __DIR__ . "/logs/";

    public static function log($filename, $str)
    {
        echo self::logPath . $filename;
        file_put_contents(self::logPath . $filename, $str, FILE_APPEND | LOCK_EX);
        //file_put_contents(self::logPath . $filename, $str, LOCK_EX);
    }
}