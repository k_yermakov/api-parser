<?php
/**
 * Created by PhpStorm.
 * User: ermakov
 * Date: 18.04.17
 * Time: 13:21
 */
error_reporting(E_ALL);
ini_set('display_errors', 'On');

class PostgresDatabase
{
    private static $connections = [];

    private static function createConnection($type)
    {
        switch ($type) {
            case 'local':
                //return pg_connect("host=localhost dbname=rosreestr user=ermakov password=Wandex1");
                return pg_connect("host=localhost dbname=kadastr user=kadastr password=cv7fGd");
        }
    }

    public static function getConnection($type)
    {
        if (!isset(self::$connections[$type])) {
            self::$connections[$type] = self::createConnection($type);
        }
        return self::$connections[$type];
    }
}