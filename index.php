<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include "PostgresDatabase.php";
include "ApiParser.php";

if (defined('STDIN')) {
    if (count($argv) == 0 || count($argv) < 7) {
        exit("Пример: sudo php index.php 1 5 1 5 6 500000
                Укажите 6 параметров: 
                argv[1] - id из таблицы cns в БД,
                argv[2] - Процент прокси, используемый из общего количества, полученных по api,
                argv[3] - Максимальное количество переподключений с данного прокси,
                argv[4] - Количество секунд ожидания при попытке соединения,
                argv[5] - Максимально позволенное количество секунд для выполнения cURL-функций,
                argv[6] - Задержка (в микросекундах между запросами) По умолчанию 500000 1сек = 1000000 микросек\n");
    }

    foreach ($argv as $argvIndex => $argvValue) {
        switch ($argvIndex) {
            case 1:
                $cadasterStart = $argvValue;
                break;
            case 2:
                $percentOfProxies = $argvValue;
                break;
            case 3:
                $maxConnectionAttempts = $argvValue;
                break;
            case 4:
                $connectionTimeout = $argvValue;
                break;
            case 5:
                $timeout = $argvValue;
                break;
            case 6:
                $delay = $argvValue;
                break;
        }
    }
}

echo "cadasterStart : $cadasterStart, 
      percentOfProxies : $percentOfProxies, 
      maxConnectionAttempts : $maxConnectionAttempts, 
      connectionTimeout : $connectionTimeout,
      timeout : $timeout \n";

$cadasters = array_values(array_unique(getCadasters($cadasterStart)));

$cadasters[0] = "77:6:5015:1066";

$proxies = getProxiesFromApi();

if (floor((count($proxies) * $percentOfProxies) / 100) < 10) {
    $cntPercentOfProxies = floor((count($proxies) * $percentOfProxies) / 100);
} else {
    $cntPercentOfProxies = floor((count($proxies) * $percentOfProxies) / 100) - (floor((count($proxies) * $percentOfProxies) / 100) % 10);
}

$parser = new ApiParser($maxConnectionAttempts, $connectionTimeout, $timeout, $cntPercentOfProxies, $proxies);
$parser->removeBadProxies();

var_dump($cntPercentOfProxies);

echo "Всего кадастров: " . count($cadasters) . "\n";

$cadasterChunks = array_chunk($cadasters, $cntPercentOfProxies, true);

//var_dump($cadasters);

foreach ($cadasterChunks as $indexChunk => $cadastersOfChunk) {
    $urls = [];
    usleep($delay);

    foreach ($cadastersOfChunk as $index => $value) {
        $cadasterValue = $value;
        $urls[] = "http://pkk5.rosreestr.ru/api/features/5/$cadasterValue";
    }

    if ($parser->getActiveProxiesCnt() < $cntPercentOfProxies) {
            $proxies = getProxiesFromApi();
            $parser->setProxies($proxies);
            $parser->removeBadProxies();
    }

    try
    {
        $content = $parser->getMultipleResponses($urls);
    }
    catch(Exception $e)
    {
        $proxies = getProxiesFromApi();
        $parser->setProxies($proxies);
        $parser->removeBadProxies();
        $content = $parser->getMultipleResponses($urls);
    }

    insertInDb($content);
}

function insertInDb($content)
{
    $conn = PostgresDatabase::getConnection('local');

    foreach ($content as $key => $json) {
        $arr = json_decode($json, true);
        if (isset($arr['feature']['attrs'])) {
            pg_query($conn, "INSERT INTO responses(response) VALUES('$json');");
        }
    }
}

function getProxiesFromApi()
{
    //$content1 = formatProxies(file_get_contents('http://api.best-proxies.ru/proxylist.txt?key=0c3edd1f3be073087b485ebf6f66b288&limit=0&level=1,2&type=http,https'));
    $content1 = [];
    //Office
    //$content2 = formatProxies(file_get_contents('http://freeproxylist.org/en/downloader.php?key=e94E7S77AZth8xVdH9A2f8TtH7IZDkzw&filter=any|any|23|any|any|any|0|0|15.0000|0|360'));
    //Server
    $content2 = formatProxies(file_get_contents("http://freeproxylist.org/en/downloader.php?key=iKB4YXkR8c49kGPh692iJqHiSQxlBUCW&filter=any|any|23|any|any|any|0|0|15.0000|0|360"));


    return array_merge($content1, $content2);
    /*return [
        "23.95.86.220:1080",
        "94.20.21.38:3128",
        "94.140.248.180:8080",
        "35.187.40.8:80",
        "93.190.253.50:80",
        "144.217.104.145:80",
        "79.143.178.84:8080",
        "78.111.92.59:8080",
        "84.72.14.196:80",
        "178.215.150.44:8080",
        "92.60.180.113:3128",
        "88.99.174.139:3128",
        "91.210.117.71:3128",
        "155.133.92.12:8080",
        "23.111.130.210:3128",
        "185.108.198.230:8080"
    ];*/
}

function sanitizeCadasters($cadasters)
{
    $result = [];
    foreach ($cadasters as $index => $cadaster) {
        $result[] = preg_replace("~[^0-9.:]~xui", "", $cadaster);
    }
    return $result;

}

function getCadasters($id = 0)
{
    $cadasters = [];
    $conn = PostgresDatabase::getConnection('local');

    $result = pg_query($conn, "SELECT  vals->'attrs'->'id' as cadaster
                               FROM  cns, json_array_elements((cns.cn->>'features')::json) as vals
                               WHERE cns.id > $id;");

    while ($row = pg_fetch_assoc($result)) {
        $cadasters[] = $row['cadaster'];
    }


    return sanitizeCadasters($cadasters);
}


function formatProxies($content)
{
    $proxies = explode("\n", $content);
    $formattedProxies = [];

    foreach ($proxies as $index => $proxy) {
        $formattedProxies[] = preg_replace("~[^0-9.:]~xui", "", $proxy);
    }

    return $formattedProxies;
}

?>