<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include "PostgresDatabase.php";
include "ApiParser.php";

checkCLIParameters($argv);

foreach ($argv as $argvIndex => $argvValue) {
    switch ($argvIndex) {
        case 1:
            $percentOfProxies = $argvValue;
            break;
        case 2:
            $maxConnectionAttempts = $argvValue;
            break;
        case 3:
            $connectionTimeout = $argvValue;
            break;
        case 4:
            $timeout = $argvValue;
            break;
        case 5:
            $cadasterStart = $argvValue;
            break;
        case 6:
            $cadasterEnd = $argvValue;
            break;
        case 7:
            $delay = $argvValue;
            break;
    }
}

echo "
      percentOfProxies : $percentOfProxies, 
      maxConnectionAttempts : $maxConnectionAttempts, 
      connectionTimeout : $connectionTimeout,
      timeout : $timeout \n";

$proxies = getProxiesFromApi();
$cntPercentOfProxies = formChunkSize($proxies, $percentOfProxies);

$parser = new ApiParser($maxConnectionAttempts, $connectionTimeout, $timeout, $cntPercentOfProxies, $proxies);
$parser->removeBadProxies();

var_dump($cntPercentOfProxies);

run($parser, $cadasterStart, $cadasterEnd, $delay, $cntPercentOfProxies);

function run($parser, $cadasterStart, $cadasterEnd, $delay, $cntPercentOfProxies)
{
    $urlsToParse = getUrlsToParse();
    foreach ($urlsToParse as $urlsToParseIndex => $urlsToParseValue) {
        for ($i = $cadasterStart; $i <= $cadasterEnd; $i += ($cntPercentOfProxies * 10)) {
            $urls = [];
            usleep($delay);

            for ($j = $i; $j <= $i + ($cntPercentOfProxies * 10); $j = $j + 10) {
                $skip = $j;
                $url = $urlsToParseValue . $skip;
                $urls[] = $url;
            }

            if ($parser->getActiveProxiesCnt() < $cntPercentOfProxies) {
                $proxies = getProxiesFromApi();
                $parser->setProxies($proxies);
                $parser->removeBadProxies();
            }

            $content = $parser->getMultipleResponses($urls);

            if (insertInDb($content) == false) {
                //выходим из цикла по данному урлу $urlsToParseValue
                break;
            }
        }
    }
}

function getUrlsToParse()
{
    $urls = [
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2029.5202637,%2060.2806853%20],%20[%2029.5037842,%2060.2834085%20],%20[%2029.432373,%2059.5231755%20],%20[%2031.0089111,%2059.5092423%20],%20[%2030.9210205,%2060.2316302%20],%20[%2029.5202637,%2060.2806853%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2082.4015808,%2055.1757309%20],%20[%2082.440033,%2054.8188875%20],%20[%2083.3354187,%2054.8370819%20],%20[%2083.3120728,%2055.2188404%20],%20[%2082.4620056,%2055.1780835%20],%20[%2082.4015808,%2055.1757309%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2060.2352905,%2056.9599522%20],%20[%2060.284729,%2056.6403714%20],%20[%2060.9466553,%2056.6381059%20],%20[%2060.8821106,%2057.0041025%20],%20[%2060.2572632,%2056.9629471%20],%20[%2060.2352905,%2056.9599522%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2043.7310791,%2056.4321296%20],%20[%2043.7722778,%2056.1822539%20],%20[%2044.1911316,%2056.1822539%20],%20[%2044.155426,%2056.445795%20],%20[%2043.7887573,%2056.4306109%20],%20[%2043.7310791,%2056.4321296%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2050.0296783,%2053.2816374%20],%20[%2050.0351715,%2053.1055678%20],%20[%2050.4581451,%2053.0981472%20],%20[%2050.4464722,%2053.2968244%20],%20[%2050.0571442,%2053.2832795%20],%20[%2050.0296783,%2053.2816374%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2073.1586456,%2055.0811187%20],%20[%2073.1689453,%2054.8734461%20],%20[%2073.5946655,%2054.8631726%20],%20[%2073.5644531,%2055.0650005%20],%20[%2073.2231903,%2055.0826908%20],%20[%2073.1586456,%2055.0811187%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2048.9440918,%2055.8949513%20],%20[%2048.9983368,%2055.674487%20],%20[%2049.4041443,%2055.6868753%20],%20[%2049.3395996,%2055.9080395%20],%20[%2049.0127563,%2055.8991862%20],%20[%2048.9440918,%2055.8949513%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2061.1993408,%2055.282635%20],%20[%2061.1965942,%2055.2568164%20],%20[%2061.2501526,%2055.0614615%20],%20[%2061.5776825,%2055.0972303%20],%20[%2061.55159,%2055.2818529%20],%20[%2061.1993408,%2055.282635%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2039.3104553,%2047.3174139%20],%20[%2039.3159485,%2047.2810923%20],%20[%2039.3955994,%2047.041118%20],%20[%2040.0437927,%2047.1206069%20],%20[%2039.9613953,%2047.3992788%20],%20[%2039.3104553,%2047.3174139%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2055.7926941,%2054.8355001%20],%20[%2055.7885742,%2054.8109743%20],%20[%2055.8132935,%2054.6221831%20],%20[%2056.2417603,%2054.6380817%20],%20[%2056.2651062,%2054.8481527%20],%20[%2055.7926941,%2054.8355001%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2044.3675995,%2048.8665215%20],%20[%2044.3675995,%2048.8416726%20],%20[%2044.3799591,%2048.5266098%20],%20[%2044.914856,%2048.5352492%20],%20[%2044.9258423,%2048.8543245%20],%20[%2044.3675995,%2048.8665215%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2056.000061,%2058.0604444%20],%20[%2056.0343933,%2057.8783561%20],%20[%2056.4848328,%2057.8841972%20],%20[%2056.434021,%2058.0807807%20],%20[%2056.0577393,%2058.0684351%20],%20[%2056.000061,%2058.0604444%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip=",
        "http://pkk5.rosreestr.ru/api/features/5?sq={%20%22coordinates%22:%20[%20[%20[%2092.7349091,%2056.0770174%20],%20[%2092.7575684,%2055.9407405%20],%20[%2093.1407166,%2055.9295868%20],%20[%2093.1736755,%2056.1065127%20],%20[%2092.7877808,%2056.0992367%20],%20[%2092.7349091,%2056.0770174%20]%20]%20],%20%22type%22:%20%22Polygon%22%20}&skip="
    ];

    return $urls;
}

function insertInDb($content)
{
    $conn = PostgresDatabase::getConnection('local');

    foreach ($content as $key => $json) {
        $arr = json_decode($json, true);
        if (isset($arr['features'])) {
            pg_query($conn, "INSERT INTO cns(cn) VALUES('$json');");
        }

        if (empty($arr['features']))//отправляем false, т.к. данные после очередного skip не приходят
        {
            return false;
        }
    }

    return true;
}

function getProxiesFromApi()
{
    $content1 = formatProxies(file_get_contents('http://api.best-proxies.ru/proxylist.txt?key=0c3edd1f3be073087b485ebf6f66b288&limit=0&level=1,2&type=http,https'));

    //Office
    //$content2 = formatProxies(file_get_contents('http://freeproxylist.org/en/downloader.php?key=e94E7S77AZth8xVdH9A2f8TtH7IZDkzw&filter=any|any|23|any|any|any|0|0|15.0000|0|360'));

    //Server
    $content2 = formatProxies(file_get_contents("http://freeproxylist.org/en/downloader.php?key=iKB4YXkR8c49kGPh692iJqHiSQxlBUCW&filter=any|any|23|any|any|any|0|0|15.0000|0|360"));

    var_dump($content2);

    return array_merge($content1, $content2);
}

function formatProxies($content)
{
    $proxies = explode("\n", $content);
    $formattedProxies = [];

    foreach ($proxies as $index => $proxy) {
        $formattedProxies[] = preg_replace("~[^0-9.:]~xui", "", $proxy);
    }

    return $formattedProxies;
}

/*
 * Функция, которая определяет количество одновременных запросов (на каждый запрос свой прокси)
 *
 */
function formChunkSize($proxies, $percentOfProxies)
{
    if (floor((count($proxies) * $percentOfProxies) / 100) < 10) {
        return floor((count($proxies) * $percentOfProxies) / 100);
    } else {
        return floor((count($proxies) * $percentOfProxies) / 100) - (floor((count($proxies) * $percentOfProxies) / 100) % 10);
    }
}

function checkCLIParameters($argv)
{
    if (defined('STDIN')) {
        if (count($argv) == 0 || count($argv) < 8) {
            exit("Пример: sudo php indexExtractCN.php 1 1 5 6 0 50000 500000
                Укажите 7 параметров: 
                argv[1] - Процент прокси, используемый из общего количества, полученных по api,
                argv[2] - Максимальное количество переподключений с данного прокси,
                argv[3] - Количество секунд ожидания при попытке соединения,
                argv[4] - Максимально позволенное количество секунд для выполнения cURL-функций,
                argv[5] - Значение, с которого начинается парсинг кадастрового квартала Пример: 77:7:1234:cadasterStart,
                argv[6] - Значение, на котором заканчивается парсинг кадастрового квартала Пример: 77:7:1234:cadasterEnd,
                argv[7] - Задержка (в микросекундах между запросами) По умолчанию 500000 1сек = 1000000 микросек\n");
        }
    }
}

?>